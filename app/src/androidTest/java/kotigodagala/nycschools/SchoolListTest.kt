package kotigodagala.nycschools


import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import kotigodagala.nycschools.view.ui.SchoolsListActivity
import org.hamcrest.CoreMatchers.anything

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class SchoolListTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(SchoolsListActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("kotigodagala.nycschools", appContext.packageName)
    }

    @Test
    fun testSchoolList() {
        onView(withId(R.id.schoolsList)).check(matches(isDisplayed()))
        onData(anything()).inAdapterView(withId(R.id.schoolsList)).atPosition(1).perform(click())
    }

    @Test
    fun testSchoolDetails() {
        onView(withId(R.id.schoolsList)).check(matches(isDisplayed()))
        onData(anything()).inAdapterView(withId(R.id.schoolsList)).atPosition(1).perform(click())
        onView(withId(R.id.sname)).check(matches(isDisplayed()))
    }

    @Test
    fun testSchoolSATNotFound() {
        onView(withId(R.id.schoolsList)).check(matches(isDisplayed()))
        onData(anything()).inAdapterView(withId(R.id.schoolsList)).atPosition(0).perform(click())
        onView(withId(R.id.reading_avg)).check(matches(withText("")))
    }


}
