package kotigodagala.nycschools

import kotigodagala.nycschools.service.model.SATScore
import kotigodagala.nycschools.service.model.School
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class SchoolUnitTest {
    @Test
    fun testSchool() {
        val school = School(
            "123", "test", "test@gmail.com", "12345",
            "test", "test"
        )
        assertEquals("123", school.dbn)
        assertEquals("test", school.school_name)
        assertEquals("test@gmail.com", school.school_email)
    }

    @Test
    fun testSATScore() {
        val satScore = SATScore("12", "1", "23", "3")
        assertEquals("12", satScore.num_of_sat_test_takers)
        assertEquals("1", satScore.sat_critical_reading_avg_score)
    }
}
