package kotigodagala.nycschools.service.model

/**
 * Class to store SAT Score
 */
class SATScore(
    val num_of_sat_test_takers: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String
)