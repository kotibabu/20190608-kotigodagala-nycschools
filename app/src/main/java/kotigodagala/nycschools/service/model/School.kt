package kotigodagala.nycschools.service.model

import java.io.Serializable

/**
 * Class to store School Details
 */
class School(
    val dbn: String,
    val school_name: String,
    val school_email: String,
    val phone_number: String,
    val academicopportunities1: String,
    val academicopportunities2: String
) : Serializable