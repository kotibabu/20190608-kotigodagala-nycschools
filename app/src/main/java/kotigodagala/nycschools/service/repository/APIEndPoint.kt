package kotigodagala.nycschools.service.repository

import kotigodagala.nycschools.service.model.SATScore
import kotigodagala.nycschools.service.model.School
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * This Class holds all the API end points
 */

interface APIEndPoint {


    @GET("s3k6-pzi2.json")
    abstract fun listSchools(): Call<List<School>>

    @GET("f9bf-2cp4.json")
    abstract fun getSATScores(@Query("dbn") dbn: String): Call<List<SATScore>>

    companion object {
        const val HTTPS_API = "https://data.cityofnewyork.us/resource/"
    }
}