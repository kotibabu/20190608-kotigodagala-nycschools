package kotigodagala.nycschools.service.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotigodagala.nycschools.service.model.SATScore
import kotigodagala.nycschools.service.model.School
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * This class holds all the API call implementations
 */
object APIService {

    private val apiEndPoint: APIEndPoint

    init {
        val retrofit =
            Retrofit.Builder().baseUrl(APIEndPoint.HTTPS_API).addConverterFactory(GsonConverterFactory.create())
                .build();
        apiEndPoint = retrofit.create(APIEndPoint::class.java)
    }

    /**
     * mutableLiveData - holds the list of schools
     * This function is to make the api call to fetch all nyc schools
     */
    fun getSchools(mutableLiveData: MutableLiveData<List<School>>) {
        apiEndPoint.listSchools().enqueue(object : Callback<List<School>> {
            override fun onFailure(call: Call<List<School>>, t: Throwable) {
                mutableLiveData.value = null
            }

            override fun onResponse(call: Call<List<School>>, response: Response<List<School>>) {
                mutableLiveData.value = response.body()
            }
        })
    }

    /**
     * schoolId - holds the school dbn value
     * mutableLiveData - holds the SATScore
     * This function is to fetch the SAT Score of the selection School
     */
    fun getSchoolDetails(schoolId: String, mutableLiveData: MutableLiveData<List<SATScore>>) {
        apiEndPoint.getSATScores(schoolId).enqueue(object : Callback<List<SATScore>> {
            override fun onFailure(call: Call<List<SATScore>>, t: Throwable) {
                mutableLiveData.value = null
            }

            override fun onResponse(call: Call<List<SATScore>>, response: Response<List<SATScore>>) {
                mutableLiveData.value = response.body()
            }
        })
    }
}