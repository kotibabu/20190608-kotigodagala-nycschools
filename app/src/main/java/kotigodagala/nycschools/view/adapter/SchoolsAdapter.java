package kotigodagala.nycschools.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import kotigodagala.nycschools.R;
import kotigodagala.nycschools.service.model.School;

import java.util.List;

/**
 * This class is to show the items on the list
 */
public class SchoolsAdapter extends BaseAdapter {

    private List<School> schoolList;


    public SchoolsAdapter(List<School> schoolList) {
        this.schoolList = schoolList;
    }

    public class ViewHolder {
        @BindView(R.id.sname)
        TextView mTextView;

        public ViewHolder(View v) {
            ButterKnife.bind(this, v);
        }
    }


    public School getSchoolFromPosition(int position) {
        return schoolList.get(position);
    }

    @Override
    public int getCount() {
        return schoolList.size();
    }

    @Override
    public Object getItem(int position) {
        return schoolList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_school, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        holder.mTextView.setText(schoolList.get(position).getSchool_name());
        return convertView;
    }
}
