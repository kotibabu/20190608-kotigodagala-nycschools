package kotigodagala.nycschools.view.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotigodagala.nycschools.R
import kotigodagala.nycschools.service.model.SATScore
import kotigodagala.nycschools.service.model.School
import kotigodagala.nycschools.viewmodel.SchoolViewModel
import kotlinx.android.synthetic.main.activity_schooldetail.*

class SchoolDetailActivity : AppCompatActivity() {

    private val schoolViewModel by lazy {
        return@lazy ViewModelProviders.of(this).get(SchoolViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schooldetail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val school: School = intent.getSerializableExtra(getString(R.string.extra_school)) as School
        getSchoolDetails(school)
        title = school.school_name
    }

    private fun getSchoolDetails(school: School) {
        schoolViewModel.getSATScore(school.dbn).observe(this, Observer { satScore ->
            progressBar.visibility = View.GONE
            if (satScore != null && satScore.isNotEmpty()) {
                setData(school, satScore[0])
            } else {
                setData(school, null)
                showError()
            }
        })
    }

    private fun showError() {
        Toast.makeText(this, getString(R.string.score_not_available), Toast.LENGTH_LONG).show()
    }

    private fun setData(school: School, satScore: SATScore?) {
        sname.text = school.school_name
        ttakers.text = satScore?.num_of_sat_test_takers ?: ""
        reading_avg.text = satScore?.sat_critical_reading_avg_score ?: ""
        math_avg.text = satScore?.sat_math_avg_score ?: ""
        writing_avg.text = satScore?.sat_writing_avg_score ?: ""
        school_email.text = school.school_email
        school_phone.text = school.phone_number
        opp1.text = school.academicopportunities1
        opp2.text = school.academicopportunities2

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

}