package kotigodagala.nycschools.view.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import kotigodagala.nycschools.R;
import kotigodagala.nycschools.service.model.School;
import kotigodagala.nycschools.view.adapter.SchoolsAdapter;
import kotigodagala.nycschools.viewmodel.SchoolViewModel;

import java.util.List;

public class SchoolsListActivity extends AppCompatActivity {
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.schoolsList)
    ListView schoolsList;
    private SchoolsAdapter schoolsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schools);
        ButterKnife.bind(this);
        setTitle(R.string.title_schools);
        showSchools();
        setOnListItemClick();
    }

    private void showSchools() {
        SchoolViewModel schoolListViewModel = ViewModelProviders.of(this).get(SchoolViewModel.class);
        schoolListViewModel.getSchools().observeForever(new Observer<List<School>>() {
            @Override
            public void onChanged(List<School> schools) {
                if (schools != null) {
                    progressBar.setVisibility(View.GONE);
                    schoolsAdapter = new SchoolsAdapter(schools);
                    schoolsList.setAdapter(schoolsAdapter);
                } else {
                    showError();
                }
            }
        });
    }

    private void showError() {
        Toast.makeText(this, getString(R.string.request_error), Toast.LENGTH_LONG).show();
    }

    private void setOnListItemClick() {
        schoolsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SchoolsListActivity.this, SchoolDetailActivity.class);
                intent.putExtra(getString(R.string.extra_school), schoolsAdapter.getSchoolFromPosition(i));
                startActivity(intent);
            }
        });
    }
}
