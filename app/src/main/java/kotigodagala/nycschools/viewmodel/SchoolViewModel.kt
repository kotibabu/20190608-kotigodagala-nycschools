package kotigodagala.nycschools.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import kotigodagala.nycschools.service.model.SATScore
import kotigodagala.nycschools.service.model.School
import kotigodagala.nycschools.service.repository.APIService

class SchoolViewModel(application: Application) : AndroidViewModel(application) {

    /**
     * This function fetchs list of schools
     */
    fun getSchools(): MutableLiveData<List<School>> {
        val schoolList: MutableLiveData<List<School>> = MutableLiveData()
        APIService.getSchools(schoolList)
        return schoolList
    }

    /**
     * dbn - Is a string which identifies the school
     * This function fetches SAT score of the subject
     */
    fun getSATScore(dbn: String): MutableLiveData<List<SATScore>> {
        var schoolDetail: MutableLiveData<List<SATScore>> = MutableLiveData()
        APIService.getSchoolDetails(dbn, schoolDetail)
        return schoolDetail
    }

}